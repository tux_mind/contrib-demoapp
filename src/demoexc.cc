/* Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "demoexc.h"

#include <algorithm>
#include <cstdio>
#include <bbque/utils/timer.h>
#include <bbque/utils/utility.h>

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.app"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetChUid()

namespace bu = bbque::utils;

DemoWorkload::DemoWorkload(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		uint32_t cycle_ms,
		uint8_t max_nthds,
		uint8_t max_awm):
	BbqueEXC(name, recipe, rtlib),
	cycle_ms(cycle_ms),
	max_num_tds(max_nthds),
	max_awm_id(max_awm) {

	if (max_num_tds > DEFAULT_MAX_NUM_THREADS)
		max_num_tds = DEFAULT_MAX_NUM_THREADS;

	awm_upper_id = max_awm_id;
	finished = upper_changed = false;
	logger->Notice("New Execution Context registered."
			"Run cycle time: %d[ms] "
			"Maximum number of threads: %d",
			cycle_ms, max_num_tds);
}

DemoWorkload::~DemoWorkload() {
	td_pool.clear();
}

void DemoWorkload::WorkFunction(uint8_t id, double run_time) {
	std::unique_lock<std::mutex> start_lk(start_mtx, std::defer_lock);
	std::unique_lock<std::mutex> run_lk(run_mtx, std::defer_lock);
	std::unique_lock<std::mutex> en_lk(tds_enable_mtx[id], std::defer_lock);
	std::unique_lock<std::mutex> done_lk(tds_done_mtx[id], std::defer_lock);
	bu::Timer tmr;
	uint32_t next_run_cycle = 1;
	double elapsed_time = 0.0;
	double var_time;

	// Total cycle time (with a 10% max std dev)
	var_time = (rand() % 10) / 100.0;
	run_time *= (1 + var_time);
	logger->Notice("Thread %d: run time = %3.0f[ms]", id, run_time);

	// Starting the thread
	start_lk.lock();
	--starting_threads;
	if (starting_threads == 0) {
		start_cv.notify_one();
	}
	start_lk.unlock();

	// Execution loop
	while (!finished) {
		// Wait to run
		run_lk.lock();
		while (next_run_cycle > run_count) {
			DB(logger->Notice("Thread %d: wait to run", id));
			run_cv.wait(run_lk);
			if (finished)
				goto signal_done;
		}
		run_lk.unlock();
		next_run_cycle++;
		DB(logger->Notice("Thread %d: cycle %d started!", id, run_count));

		// If not enabled jump to "done"
		en_lk.lock();
		if (id >= high_active_thread_id) {
			en_lk.unlock();
			DB(logger->Notice("Thread %d: inactive (active id: %d)", id, high_active_thread_id));
			goto signal_done;
		}
		en_lk.unlock();

		// ... Timed busy loop ...
		tmr.start();
		elapsed_time = 0.0;
		while (elapsed_time <= run_time) {
			//usleep(180000);
			elapsed_time = tmr.getElapsedTimeMs();
		}
		tmr.stop();

signal_done:
		// Signal done
		DB(logger->Notice("Thread %d: cycle %d finishing...", id, run_count));
		done_lk.lock();
		tds_done[id] = true;
		tds_done_cv[id].notify_one();
		done_lk.unlock();
		DB(logger->Notice("Thread %d: cycle %d done!", id, run_count));
	}

	logger->Notice("Thread %d: exiting...", id);
}

RTLIB_ExitCode_t DemoWorkload::onSetup() {
	std::unique_lock<std::mutex> st_ul(start_mtx);
	logger->Notice("Setup: Spawning %d threads", max_num_tds);

	// Spawn max num threads
	for (int j = 0; j < max_num_tds; ++j)
		td_pool.push_back(
				std::thread(&DemoWorkload::WorkFunction, this, j, cycle_ms));

	// Wait for all threads to be ready
	while (starting_threads > 0)
		start_cv.wait(st_ul);

	logger->Notice("Setup: All %d threads are ready", max_num_tds);
	return RTLIB_OK;
}

RTLIB_ExitCode_t DemoWorkload::onConfigure(int8_t awm_id) {
	logger->Notice("Configure: Assigned AWM %02d", awm_id);

	// Set the "active threshold" ID according to the AWM
	if (awm_id < 0)
		high_active_thread_id = max_num_tds;
	else
		high_active_thread_id = std::min<int>(max_num_tds, awm_id*2);
	logger->Notice("Configure: Active thread/s = %d", high_active_thread_id);

	return RTLIB_OK;
}

RTLIB_ExitCode_t DemoWorkload::onRun() {
	std::unique_lock<std::mutex> run_lk(run_mtx);

	// Clean thread done flags
	for (int i = max_num_tds-1; i >= 0; --i)
		tds_done[i] = false;

	// Inc cycle count and notify threads for running
	run_count++;
	run_cv.notify_all();
	DB(logger->Notice("Run: cycle:%d. Start signaled!", run_count));
	run_lk.unlock();

	// Wait for all threads to signal "done"
	for (int i = 0; i < max_num_tds; ++i) {
		std::unique_lock<std::mutex> done_lk(tds_done_mtx[i]);
		logger->Notice("Run: Waiting for thread %d to be done", i);
		if (!tds_done[i])
			tds_done_cv[i].wait(done_lk);
		DB(logger->Notice("Run: Thread %d done", i));
	}

	// Continue
	if (!finished)
		return RTLIB_OK;

	// Finished: return workload none
	return RTLIB_EXC_WORKLOAD_NONE;
}

RTLIB_ExitCode_t DemoWorkload::onMonitor() {
	RTLIB_Constraint_t cnstr = {awm_upper_id, CONSTRAINT_ADD, UPPER_BOUND};

	// If the upper bound has changed, assert the constraint
	if (upper_changed) {
		logger->Notice("Maximum AWM assignable changed into: %d", awm_upper_id);
		SetConstraints(&cnstr, 1);
	}

	upper_changed = false;
	return RTLIB_OK;
}

RTLIB_ExitCode_t DemoWorkload::onRelease() {

	// Finished: join threads
	logger->Notice("Run: Joining threads...");
	finished = true;
	run_cv.notify_all();
	for (int i = 0; i < max_num_tds; ++i) {
		td_pool[i].join();
		logger->Notice("Run: ...thread %d joint", i);
	}

	return RTLIB_OK;

}

RTLIB_ExitCode_t DemoWorkload::onSuspend() {
	return RTLIB_OK;
}

void DemoWorkload::SetFinished() {
	finished = true;
}

void DemoWorkload::IncUpperAwmID() {
	if (awm_upper_id == max_awm_id)
		return;

	++awm_upper_id;
	upper_changed = true;
	logger->Notice("Assignable AWMs: [0, %d]", awm_upper_id);
}

void DemoWorkload::DecUpperAwmID() {
	if (awm_upper_id == 0)
		return;

	--awm_upper_id;
	upper_changed = true;
	logger->Notice("Assignable AWMs: [0, %d]", awm_upper_id);
}

