
ifdef CONFIG_CONTRIB_TESTING_DEMOAPP

# Targets provided by this project
.PHONY: rtlib_demoapp clean_rtlib_demoapp

# Add this to the "contrib_testing" target
testing: rtlib_demoapp
clean_testing: clean_rtlib_demoapp

MODULE_CONTRIB_TESTING_DEMOAPP=contrib/testing/rtlib-demoapp

rtlib_demoapp: external
	@echo
	@echo "==== Building Demo Application ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_CONTRIB_TESTING_DEMOAPP)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_TESTING_DEMOAPP)/build/$(BUILD_TYPE) || \
		exit 1
	cd $(MODULE_CONTRIB_TESTING_DEMOAPP)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_TESTING_DEMOAPP)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_rtlib_demoapp:
	@echo
	@echo "==== Clean-up Demo Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-demoapp ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqDemo*; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-demoapp*
	@rm -rf $(MODULE_CONTRIB_TESTING_DEMOAPP)/build
	@echo

else # CONFIG_CONTRIB_TESTING_DEMOAPP

demoapp:
	$(warning contib DemoApp module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_TESTING_DEMOAPP

